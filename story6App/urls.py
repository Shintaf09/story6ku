from django.urls import path, include
from django.contrib import admin
from .views import *

app_name = 'tugasKelompokApp'

urlpatterns = [
    path('', index, name='index'),
    path('status/', index, name="index"),
    path('submitstatus/', submitstatus, name="submit"),
]