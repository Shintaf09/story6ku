from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest
from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.
class UnitTestStory6 (TestCase):
	# unit testing
	def test_apakah_ada_url_status (self):
		response = Client().get('/status/')
		self.assertEqual (response.status_code,200)
	def test_apakah_templatenya_pakai_landingpage_html (self) :
		response = Client().get('/status/')
		self.assertTemplateUsed (response, 'landingpage.html')
	def test_apakah_nama_fungsi_diviewsnya_index (self):
		found = resolve ('/status/')
		self.assertEqual(found.func, index)
	def test_apakah_ada_tulisan_HaloApaKabar (self):
		request = HttpRequest()
		response = index (request)
		html_response = response.content.decode('utf8')
		self.assertIn ('Halo, apa kabar?', html_response)
	def test_apakah_formnya_ada (self):
		request = HttpRequest()
		response = index (request)
		html_response = response.content.decode('utf8')
		self.assertIn ('</form>', html_response)
	def test_apakah_url_wek_tidak_ada (self):
		response = Client().get('/wek/')
		self.assertEqual (response.status_code,404)
	def test_submit_status(self):
		status = 'Halo halo Bandung'
		response = Client().post('/submitstatus/', {'status' : status})
		response = Client().get('/status/')
		self.assertIn(status, response.content.decode())

class FunctionalTestStory6 (TestCase):
	# functional testing
	def setUp(self):
		super().setUp()
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		self.browser  = webdriver.Chrome(chrome_options=chrome_options)

	def tearDown(self):
		self.browser.quit()
		super().tearDown()

	def test_apakah_input_cobacoba_jika_disubmit_outputnya_juga_cobacoba(self) :
		self.browser.get ('http://127.0.0.1:8000/')
		input_form = self.browser.find_element_by_name('status')
		submit = self.browser.find_element_by_id('submitan')
		input_form.send_keys('cobacoba')
		time.sleep(4)
		submit.send_keys(Keys.RETURN)
		self.browser.get ('http://127.0.0.1:8000/')
		self.assertIn('cobacoba', self.browser.page_source)