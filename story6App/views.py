from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import StatusModels

# Create your views here.
def index(request):
	list_Status = StatusModels.objects.all().order_by('-tanggal')
	return render(request,"landingpage.html", {'List_Status_Key' : list_Status})
def submitstatus (request):
	if request.method == 'POST' : 
		form = StatusForm(request.POST)
		if form.is_valid():
			status = request.POST ['status']
			listku = StatusModels(status=status)
			listku.save()
	return redirect('/status')
